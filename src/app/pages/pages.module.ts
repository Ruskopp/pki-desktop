import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { HomeComponent } from './home/home.component';
import { OrdersComponent } from './orders/orders.component';
import { ReviewComponent } from './review/review.component';
import { RegisterComponent } from './register/register.component';
import { OrderComponent } from './order/order.component';

const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
  ],
  declarations: [
    ...PAGES_COMPONENTS,
    HomeComponent,
    OrdersComponent,
    ReviewComponent,
    RegisterComponent,
    OrderComponent,
  ],
  entryComponents: [
    ReviewComponent,
  ],
})
export class PagesModule {
}

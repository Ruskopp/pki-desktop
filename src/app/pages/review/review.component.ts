import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngx-review',
  templateUrl: './review.component.html',
// tslint:disable-next-line:trailing-comma
  styleUrls: ['./review.comonent.scss']
})
export class ReviewComponent implements OnInit {
  heartRate = 3;

  constructor(private activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

  closeModal() {
    this.activeModal.close();
  }

}

import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ReviewComponent } from '../review/review.component';

@Component({
  selector: 'ngx-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss'],
})
export class OrdersComponent implements OnInit {

  orders: any[];

  constructor(private modalService: NgbModal) {
  }

  showReview() {
    const activeModal = this.modalService.open(ReviewComponent, { size: 'lg', container: 'nb-layout' });

    activeModal.componentInstance.modalHeader = 'Large Modal';
  }

  ngOnInit() {
    this.orders = [{
      restaurant: 'Butik',
      date: '25.06.2018',
      food: [
        {
          title: 'Pizza',
          amount: '2',
          price: '358',
        }, {
          title: 'Pizza',
          amount: '2',
          price: '358',
        }, {
          title: 'Pizza',
          amount: '2',
          price: '358',
        }, {
          title: 'Pizza',
          amount: '2',
          price: '358',
        }, {
          title: 'Pizza',
          amount: '2',
          price: '358',
        },
      ],
      totalPrice: '358',
    },
      {
        restaurant: 'Butik',
        date: '25.06.2018',
        food: [
          {
            title: 'Pizza',
            amount: '2',
            price: '358',
          }, {
            title: 'Pizza',
            amount: '2',
            price: '358',
          }, {
            title: 'Pizza',
            amount: '2',
            price: '358',
          }, {
            title: 'Pizza',
            amount: '2',
            price: '358',
          }, {
            title: 'Pizza',
            amount: '2',
            price: '358',
          },
        ],
        totalPrice: '358',
      },
      {
        restaurant: 'Butik',
        date: '25.06.2018',
        food: [
          {
            title: 'Pizza',
            amount: '2',
            price: '358',
          }, {
            title: 'Pizza',
            amount: '2',
            price: '358',
          }, {
            title: 'Pizza',
            amount: '2',
            price: '358',
          }, {
            title: 'Pizza',
            amount: '2',
            price: '358',
          }, {
            title: 'Pizza',
            amount: '2',
            price: '358',
          },
        ],
        totalPrice: '358',
      },
      {
        restaurant: 'Butik',
        date: '25.06.2018',
        food: [
          {
            title: 'Pizza',
            amount: '2',
            price: '358',
          }, {
            title: 'Pizza',
            amount: '2',
            price: '358',
          }, {
            title: 'Pizza',
            amount: '2',
            price: '358',
          }, {
            title: 'Pizza',
            amount: '2',
            price: '358',
          }, {
            title: 'Pizza',
            amount: '2',
            price: '358',
          },
        ],
        totalPrice: '358',
      }]
  }

}
